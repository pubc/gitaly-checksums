package main

import (
	"flag"
	"fmt"
	"gitaly-checksums/job"
	"gitaly-checksums/repo"
	"log"
	_ "net/http/pprof"
	"runtime"
	"time"
)

func main() {
	// read workers number from terminal
	jobs := flag.Int("j", runtime.NumCPU()/2, "jobs")
	// read git data from terminal
	repos := flag.String("d", "/var/opt/gitlab/git-data", "dirs")
	// primary or secondary
	endpoint := flag.String("e", "primary", "'primary' or 'secondary'")
	project := flag.Int("p", 0, "project id")
	// parse the flags
	flag.Parse()

	// **********************************
	ins := repo.Instance{
		Dir:       *repos,
		DirChan:   make(chan string, 5000),
		HashChan:  make(chan string, 1000),
		QueryChan: make(chan string, 1000),
		ExitChan:  make(chan int, 1),
		ProjectId: *project,
	}

	//go func() {
	//	fmt.Println("Starting HTTP server for pprof at :6060")
	//	if err := http.ListenAndServe(":6060", nil); err != nil {
	//		fmt.Printf("HTTP server failed: %v\n", err)
	//	}
	//}()
	//go tool pprof -seconds=10 -http=:9999 http://192.168.18.24:6060/debug/pprof/heap

	// feed DirChan
	log.Println("Git repositories checksum start ...")

	pool := job.NewPool(jobs)
	// start to calculate
	if *endpoint == "primary" {
		go ins.ListRepos()
		for {
			select {
			case path := <-ins.DirChan:
				if path != "" {
					j := func(path string) {
						ins.HashedRepo(path)
					}
					pool.AddJob(j, path)
				}
			case hashed := <-ins.HashChan:
				fmt.Println(hashed)
			case <-ins.ExitChan:
				pool.Wait()
				_, ok := <-pool.JobQueue
				if !ok && len(ins.HashChan) == 0 {
					log.Println("Git repositories checksum completed.")
					return
				}
			case <-time.After(time.Millisecond * 500):
				if len(ins.DirChan) == 0 {
					ins.ExitChan <- 0
				}
			}
		}
	} else if *endpoint == "secondary" {
		go ins.Verify(pool)
		for {
			select {
			case res := <-ins.QueryChan:
				fmt.Println(res)
			case <-ins.ExitChan:
				log.Println("Git repositories checksum completed.")
				return
			case <-ins.HashChan:
			case <-time.After(time.Millisecond * 500):
			}
		}
	} else {
		fmt.Println("Error input.")
	}
}
