package util

import (
	"log"
	"os"
	"strings"
)

func LeftPadWithDot(input string, total int) string {
	padLength := total - len(input)
	if padLength <= 0 {
		return input
	}

	pad := strings.Repeat(".", padLength)
	return pad + input
}

func Record(file, data string) {
	f, err := os.OpenFile(file, os.O_WRONLY|os.O_CREATE|os.O_APPEND, os.ModePerm)
	if err != nil {
		log.Fatalf("Create record file failed: %s\n", err)
	}
	defer f.Close()

	_, err = f.Write([]byte(data + "\n"))
	if err != nil {
		log.Printf("Write record file failed: %s\n", err)
	}

}
