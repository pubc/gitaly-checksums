module gitaly-checksums

go 1.21

require (
	github.com/lib/pq v1.10.9 // indirect
	github.com/pelletier/go-toml v1.9.5 // indirect
)
