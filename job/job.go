package job

import (
	"gitaly-checksums/db"
	"sync"
)

type Job func()

type Pool struct {
	JobQueue chan Job
	wg       sync.WaitGroup
}

var closeOnce sync.Once

// NewPool creates a new pool with multiple workers
func NewPool(maxWorkers *int) *Pool {
	p := Pool{JobQueue: make(chan Job)}
	p.wg.Add(*maxWorkers)

	for i := 0; i < *maxWorkers; i++ {
		go func() {
			defer p.wg.Done()
			for job := range p.JobQueue {
				job()
			}
		}()
	}
	return &p
}

// AddJob adds a job to the pool worker queue
func (p *Pool) AddJob(job func(s string), arg string) {
	p.JobQueue <- func() { job(arg) }
}

func (p *Pool) AddJobQuery(job func(s db.QueryResult), arg db.QueryResult) {
	p.JobQueue <- func() { job(arg) }
}

// Wait Wait to complete
func (p *Pool) Wait() {
	closeOnce.Do(func() {
		close(p.JobQueue)
	})
	p.wg.Wait()
}
