package db

import (
	"database/sql"
	"fmt"
	_ "github.com/lib/pq"
	"log"
)

func Query(instanceName string) []QueryResult {

	switch {
	case Conf.Primary.Instance.Type == "omnibus" && instanceName == "primary":
		return GetHashedPathOmnibus(Conf.Primary.Database)
	case Conf.Primary.Instance.Type == "ha" && instanceName == "primary":
		return GetHashedPathPraefect(Conf.Primary.Database)
	case Conf.Secondary.Instance.Type == "omnibus" && instanceName == "secondary":
		return GetHashedPathOmnibus(Conf.Secondary.Database)
	case Conf.Secondary.Instance.Type == "ha" && instanceName == "secondary":
		return GetHashedPathPraefect(Conf.Secondary.Database)
	}
	return nil
}

func GetHashedPathOmnibus(dbc Database) []QueryResult {
	// 设置 PostgreSQL 连接信息
	connStr := fmt.Sprintf("user=%s password=%s dbname=%s host=%s port=%d sslmode=disable", dbc.User, dbc.Pass, dbc.Name, dbc.Host, dbc.Port)

	// 连接到 PostgreSQL 数据库
	db, err := sql.Open("postgres", connStr)
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()

	// 测试连接是否成功
	err = db.Ping()
	if err != nil {
		log.Fatal("Error connecting to the database: ", err)
	}

	// 执行查询
	rows, err := db.Query(SqlOmnibus)
	if err != nil {
		log.Fatal("Error executing query: ", err)
	}
	defer rows.Close()

	var results []QueryResult
	// 处理查询结果
	for rows.Next() {
		var (
			ObjectId     int
			ObjectPath   string
			ObjectType   string
			RelativePath string
			// 为查询结果中的每一列声明变量，类型要与数据库中的列类型匹配
		)
		err := rows.Scan(&ObjectId, &ObjectPath, &ObjectType, &RelativePath)
		if err != nil {
			log.Fatal("Error scanning row: ", err)
		}
		// 对查询结果进行处理
		results = append(results, QueryResult{ObjectId, ObjectPath, ObjectType, RelativePath, "", "", ""})
	}
	// 检查是否出现迭代错误
	if err = rows.Err(); err != nil {
		log.Fatal("Error iterating rows: ", err)
	}
	return results
}

func GetHashedPathPraefect(dbc Database) []QueryResult {
	// 设置 PostgreSQL 连接信息
	connStr := fmt.Sprintf("user=%s password=%s dbname=%s host=%s port=%d sslmode=disable", dbc.User, dbc.Pass, dbc.Name, dbc.Host, dbc.Port)

	// 连接到 PostgreSQL 数据库
	db, err := sql.Open("postgres", connStr)
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()

	// 测试连接是否成功
	err = db.Ping()
	if err != nil {
		log.Fatal("Error connecting to the database: ", err)
	}

	// 执行查询
	rows, err := db.Query(SqlPraefect)
	if err != nil {
		log.Fatal("Error executing query: ", err)
	}
	defer rows.Close()

	var results []QueryResult
	// 处理查询结果
	for rows.Next() {
		var (
			RelativePath string
			ReplicaPath  string
			// 为查询结果中的每一列声明变量，类型要与数据库中的列类型匹配
		)
		err := rows.Scan(&RelativePath, &ReplicaPath)
		if err != nil {
			log.Fatal("Error scanning row: ", err)
		}
		// 对查询结果进行处理
		results = append(results, QueryResult{RelativePath: RelativePath, ReplicaPath: ReplicaPath})
	}
	// 检查是否出现迭代错误
	if err = rows.Err(); err != nil {
		log.Fatal("Error iterating rows: ", err)
	}

	return results
}
