package db

import (
	"github.com/pelletier/go-toml"
	"log"
	"os"
)

type QueryResult struct {
	ObjectId     int
	ObjectPath   string
	ObjectType   string
	RelativePath string
	ReplicaPath  string
	FullPath     string
	Sha256Sum    string
}

type Instance struct {
	Name string `toml:"name"`
	Type string `toml:"type"`
}

type Database struct {
	Host string `toml:"host"`
	Port int64  `toml:"port"`
	Name string `toml:"name"`
	User string `toml:"user"`
	Pass string `toml:"password"`
}

type Git struct {
	GitDataDirs string `toml:"git_data_dirs"`
}

type Config struct {
	Primary struct {
		Instance Instance
		Git      Git
		Database Database
	}
	Secondary struct {
		Instance Instance
		Git      Git
		Database Database
	}
}

var Conf Config

func init() {
	configFile := "config.toml"
	// 检查文件是否存在
	if _, err := os.Stat(configFile); os.IsNotExist(err) {
		// 文件不存在，创建一个空文件
		file, err := os.Create(configFile)
		if err != nil {
			log.Fatalf("Unable to create nil configuration file config.toml: %s", err)
			return
		}
		defer file.Close()
	}
	// 读取TOML文件
	config, err := toml.LoadFile(configFile)
	if err != nil {
		log.Fatalf("Error loading config file: %v", err)
	}

	// 将TOML数据解析到结构体中
	if err := config.Unmarshal(&Conf); err != nil {
		log.Fatalf("Error unmarshaling config file: %v", err)
	}
}
