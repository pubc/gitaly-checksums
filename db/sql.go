package db

var SqlOmnibus string = `WITH sub AS (
    SELECT
        project_id object_id,
        'Project' object_type,
        disk_path||'.git' relative_path
    FROM
        project_repositories
    UNION
    SELECT
        project_id,
        'Project',
        disk_path||'.wiki.git'
    FROM
        project_repositories
    UNION
    SELECT
        snippet_id,
        'Project',
        disk_path||'.git'
    FROM
        snippet_repositories
    UNION
    SELECT
        group_id,
        'Namespace',
        disk_path||'.wiki.git'
    FROM
        group_wiki_repositories
    UNION
    SELECT
        project_id,
        'Project',
        disk_path||'.design.git'
    FROM
        project_repositories
    WHERE
        project_id IN (
            SELECT
                project_id
            FROM
                design_management_repositories))
SELECT
    s.object_id,
    r.path,
    s.object_type,
    s.relative_path
FROM
    sub s,
    routes r
WHERE
    s.object_id = r.source_id
	AND s.object_type = r.source_type
ORDER BY
	3,1,2`

var SqlPraefect string = `SELECT relative_path,replica_path FROM repositories ORDER BY 1;`
