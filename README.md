# Gitaly Checkums
用于校验GitLab中所有仓库的HASH值，主要用于比较主站点和Geo站点的仓库是否一致。

## 使用说明

1. 下载 [gitaly-checksums](https://gitlab.com/pubc/gitaly-checksums/-/releases)，并按照如下操作校验仓库

2. 生成主站点所有仓库哈希

    ```shell
    # 主站点执行
    ./gitaly-checksums -d /var/opt/gitlab/git-data/repositories -j 8 -e primary | tee primary.sha256
    ```

3. 在从站点比对结果

    1. 传输文件 primary.sha256 到辅助站点

    2. 创建参数文件`config.toml`

       *以下所有参数都要填写。*

        - 如果站点是`Omnibus`，`[].database`部分写连接到 `Main` 数据库的信息。
        - 如果站点是`HA`集群，`[].database`部分写连接到 `Praefect-DB` 数据库的信息。
        - `[].instance.name`保持不变。

        ```toml
        [primary]
            [primary.instance]
            name = "primary"
            type = "omnibus"
            
            [primary.git]
            git_data_dirs = "/var/opt/gitlab/git-data"
            
            [primary.database]
            host = "192.168.117.24"
            port = 5432
            name = "gitlabhq_production_main"
            user = "gitlab"
            password = "Gitlab123"
        
        [secondary]
            [secondary.instance]
            name = "secondary"
            type = "ha"
        
            [secondary.git]
            git_data_dirs = "/var/opt/gitlab/git-data"
        
            [secondary.database]
            host = "192.168.117.24"
            port = 5432
            name = "praefect_production"
            user = "praefect"
            password = "Praefect123"
        ```

    3. 比对结果

        ```shell
        ./gitaly-checksums -d /var/opt/gitlab/git-data/repositories -e secondary | tee secondary.sha256
        ```

    4. 过滤不同步的仓库

         ```shell
         egrep '\[NO\]' secondary.sha256 | egrep -v "wiki|design"
         ```

    5. 手动重新同步

         ```shell
         # 获取不同步项目的 ID 列表
         egrep '\[NO\]' secondary.sha256 | egrep -v "wiki|design" | awk '{print $8}' | tr '\n' ',' | sed 's/,$//'
         
         # 发起同步
         projects = Project.where('id in (<project_id_list_here>)')
         projects.find_each do |project|
           puts "resync #{project.id} - #{project.name}"
           Geo::RepositorySyncService.new(project).execute
         end; nil
         
         # 确认同步完成
         repository_registries = Geo::ProjectRegistry.where('project_id in (<project_id_list_here>)')
         repository_registries.find_each do |registry|
           if registry
             puts "#{registry.project_id} - last: #{registry.last_repository_synced_at} last successful: #{registry.last_repository_successful_sync_at}"
           else
             puts "cannot find sync state."
           end
         end; nil
         ```

    6. 重新校验

         ```shell
         # 从节点操作
         ./gitaly-checksums -d /var/opt/gitlab/git-data/repositories -e secondary | tee secondary.sha256
         ```

4. 手动 rsync 不全
