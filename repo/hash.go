package repo

import (
	"errors"
	"fmt"
	"os/exec"
	"regexp"
	"sync/atomic"
	"time"
)

var globalCounter1, globalCounter2 int64

func (ins *Instance) HashedRepo(path string) string {

	// define start timestamp
	startTime := time.Now()

	// get all refs and objects
	objects := revObjectsListCmd(path)
	refs := showRefsCmd(path)
	// merge above two slice
	collection := append(objects, refs...)
	sha256sum := sha256sum(collection)

	// using short path
	reg := regexp.MustCompile(`.*@`)
	outPath := reg.ReplaceAllString(path, "@")

	// redirect to channel
	res := fmt.Sprintf("%-5.5d - %s ==> %s elapsed: %dms", getNextID1(), outPath, sha256sum, time.Since(startTime).Milliseconds())

	ins.HashChan <- res

	return sha256sum
}

// revObjectsListCmd - receive all objects
func revObjectsListCmd(repo string) []byte {
	c := fmt.Sprintf("/opt/gitlab/embedded/bin/git -C %s rev-list --all --objects | sha256sum", repo)
	cmd := exec.Command("su", "git", "-c", c)
	result, err := cmd.CombinedOutput()
	if err != nil {
		var exitError *exec.ExitError
		if errors.As(err, &exitError) {
			if exitError.ExitCode() != 0 {
				return nil
			}
		}
	}
	return result
}

// showRefsCmd - receive all refs
func showRefsCmd(repo string) []byte {
	c := fmt.Sprintf("/opt/gitlab/embedded/bin/git -C %s show-ref | sha256sum", repo)
	cmd := exec.Command("su", "git", "-c", c)
	result, err := cmd.CombinedOutput()
	if err != nil {
		var exitError *exec.ExitError
		if errors.As(err, &exitError) {
			if exitError.ExitCode() != 0 {
				return nil
			}
		}
	}
	return result
}

// getNextID1 创建全局唯一序号
func getNextID1() int64 {
	return atomic.AddInt64(&globalCounter1, 1)
}

// getNextID1 创建全局唯一序号
func getNextID2() int64 {
	return atomic.AddInt64(&globalCounter2, 1)
}

//// revObjectsList - receive all objects
//func revObjectsList(repo *git.Repository) []string {
//	objs, err := repo.Objects()
//	if err != nil {
//		log.Printf("receive objects from %s failed.\n", repo)
//	}
//
//	var objects []string
//	for {
//		obj, err := objs.Next()
//		if err != nil {
//			break
//		}
//		if !obj.ID().IsZero() && obj.Type().Valid() {
//			objects = append(objects, obj.ID().String()+" "+obj.Type().String())
//		}
//	}
//
//	return objects
//}
//
//// showRefs - git show-ref
//func showRefs(repo *git.Repository) []string {
//	refs, err := repo.References()
//	if err != nil {
//		log.Printf("show refs from %s failed.\n", repo)
//	}
//
//	var targetHash []string
//	err = refs.ForEach(func(ref *plumbing.Reference) error {
//		if !ref.Hash().IsZero() {
//			targetHash = append(targetHash, ref.Hash().String()+" "+ref.Name().String())
//		}
//		return nil
//	})
//	if err != nil {
//		log.Printf("range refs for %s failed.\n", repo)
//	}
//
//	return targetHash
//}
