package repo

import (
	"crypto/sha256"
	"encoding/hex"
)

func sha256sum(b []byte) string {
	hashed := sha256.New()
	hashed.Write(b)
	sum := hashed.Sum(nil)
	return hex.EncodeToString(sum)
}
