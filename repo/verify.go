package repo

import (
	"fmt"
	"gitaly-checksums/db"
	"gitaly-checksums/job"
	"gitaly-checksums/util"
	"log"
	"os"
	"strings"
	"time"
)

var HashedMap map[string]string = make(map[string]string)

func init() {
	bytes, err := os.ReadFile("primary.sha256")
	if err != nil {
		log.Fatalf("Cannot load file 'primary.sha256': %s\n", err)
	}

	repos := strings.Split(string(bytes), "\n")
	for _, repo := range repos {
		lines := strings.Split(repo, " ")
		if len(lines) == 7 {
			HashedMap[lines[2]] = lines[4]
		}
	}
}

func (ins *Instance) Verify(pool *job.Pool) {
	// 假设在从节点验证
	resPrimary := db.Query("primary")
	resSecondary := db.Query("secondary")

	secondaryMap := make(map[string]db.QueryResult)
	for _, sec := range resSecondary {
		secondaryMap[sec.RelativePath] = sec
	}

	for _, pri := range resPrimary {

		if pri.ObjectId != ins.ProjectId && ins.ProjectId != 0 {
			continue
		}

		if db.Conf.Primary.Instance.Type == "ha" && db.Conf.Secondary.Instance.Type == "omnibus" {
			rPath := pri.ReplicaPath
			pri = secondaryMap[pri.RelativePath]
			pri.ReplicaPath = rPath
		}

		// git-data in ha and omnibus is not the same
		// omnibus: /var/opt/gitlab/git-data/repositories/@../../../.
		// ha: /var/opt/gitlab/git-data/@../../../.
		if db.Conf.Secondary.Instance.Type == "omnibus" {
			pri.FullPath = db.Conf.Secondary.Git.GitDataDirs + "/repositories/" + secondaryMap[pri.RelativePath].RelativePath
		} else if db.Conf.Secondary.Instance.Type == "ha" {
			pri.FullPath = db.Conf.Secondary.Git.GitDataDirs + "/" + secondaryMap[pri.RelativePath].ReplicaPath
		} else {
			log.Fatalf("incorrect instance_type for secondary: %s\n", db.Conf.Secondary.Instance.Type)
		}

		// retrieve primary repos sha256sum from 'primary.sha256'
		pri.Sha256Sum = HashedMap[pri.RelativePath]

		j := func(qr db.QueryResult) {
			ins.VerifyDiff(qr)
		}
		pool.AddJobQuery(j, pri)
	}

	pool.Wait()
	time.Sleep(2 * time.Second)
	ins.ExitChan <- 0
}

func (ins *Instance) VerifyDiff(qr db.QueryResult) {
	var result string
	secHash := ins.HashedRepo(qr.FullPath)

	if secHash == qr.Sha256Sum {
		result = fmt.Sprintf("%-5.5d. [OK] - %s => %s - %d - %s", getNextID2(), qr.RelativePath, qr.Sha256Sum, qr.ObjectId, qr.ObjectPath)
	} else {
		_, err := os.Stat(qr.FullPath)
		if err != nil {
			result = fmt.Sprintf("%-5.5d. [NO] - %s => %s - %d -%s", getNextID2(), qr.RelativePath, "No repository", qr.ObjectId, qr.ObjectPath)
		} else {
			diff := fmt.Sprintf(
				"Details:\n\t"+
					"ProjectId: %d\n\t"+
					"RelativePath: %s\n\t"+
					"ReplicaPath: %s\n\t"+
					"FullPath: %s\n\t"+
					"Sha256sum: %s\n\t"+
					"Sha256sumCur: %s\n\t"+
					"Sha256sumRef: %s\t"+
					"Sha256sumObj: %s",
				qr.ObjectId,
				qr.RelativePath,
				qr.ReplicaPath,
				qr.FullPath,
				qr.Sha256Sum,
				secHash,
				string(showRefsCmd(qr.FullPath)),
				string(revObjectsListCmd(qr.FullPath)),
			)
			util.Record("./diff.lst", diff)
			result = fmt.Sprintf("%-5.5d. [NO] - %s => %s - %d -%s", getNextID2(), qr.RelativePath, secHash, qr.ObjectId, qr.ObjectPath)
		}
	}
	ins.QueryChan <- result
}
