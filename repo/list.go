package repo

import (
	"io/fs"
	"log"
	"path/filepath"
	"regexp"
	"strings"
)

func (ins *Instance) ListRepos() {

	if ins.Dir == "" {
		log.Printf("wrong path %s specified.\n", ins.Dir)
	}

	err := filepath.WalkDir(
		ins.Dir,
		func(path string, d fs.DirEntry, err error) error {
			if err != nil {
				return err
			}

			sep := string(filepath.Separator)
			scanDepth := strings.Count(path, sep) - strings.Count(ins.Dir, sep)

			if scanDepth > 5 {
				return filepath.SkipDir
			}
			reg := regexp.MustCompile(`@.*/\w{2}/\w{2}/\w{64}(.wiki)?(.design)?.git$|@cluster/(repositories|pools)/\w{2}/\w{2}/\d+$`)
			if d.IsDir() && reg.MatchString(path) {
				ins.DirChan <- path
			}
			return nil
		},
	)

	if err != nil {
		log.Fatalf("walk path %s failed: %s\n", ins.Dir, err)
	}
}
