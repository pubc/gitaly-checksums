package repo

type Instance struct {
	Jobs      int         `json:"job"`
	Dir       string      `json:"directory"`
	DirChan   chan string `json:"-"`
	QueryChan chan string `json:"-"`
	HashChan  chan string `json:"-"`
	ExitChan  chan int    `json:"-"`
	ProjectId int         `json:"-"`
}
